import UIKit

extension UIColor {
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    // App Colors
    static let navy = rgb(red: 71, green: 96, blue: 114)
    static let newBlue = rgb(red: 84, green: 140, blue: 168)
    
}


