import UIKit
import Kingfisher

class NewsDetailViewController: UIViewController, PassingDataDelegate {
    
    
    
    //MARK: - Outlets
    
    let newsImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let authorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UITextView = {
        let label = UITextView()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    //MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(newsImage)
        view.addSubview(authorLabel)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        
        setupLayout()
    }
    
    //MARK: - methods
    
    func passingData(articleVM: ArticleListViewModel) {
//        let articleVM = ArticleListViewModel(articles: article)
    }
    
    public func setupImage(with news: Article) {
        if let url = URL(string: news.urlToImage!) {
            newsImage.kf.indicatorType = .activity
            newsImage.kf.setImage(with: url)
        } else {
            newsImage.image = nil
        }
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            newsImage.heightAnchor.constraint(equalToConstant: 250),
            newsImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
            newsImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            newsImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            
            authorLabel.topAnchor.constraint(equalTo: newsImage.bottomAnchor, constant: 20),
            authorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            authorLabel.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: 20),
            
            titleLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            titleLabel.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: 20),
            
            descriptionLabel.heightAnchor.constraint(equalToConstant: 250),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)            
        ])
    }

}
