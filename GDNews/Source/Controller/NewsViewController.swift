import UIKit

protocol NewsListDelegate {
    func didTouch(cell: NewsTableViewCell, indexPath: IndexPath)
}

protocol PassingDataDelegate {
    func passingData(articleVM: ArticleListViewModel)
}

class NewsViewController: UIViewController {
    
    //MARK: - Delegate
    
    var delegate: NewsListDelegate?
    
    //MARK: - properties
    
    private let cellIdentifier = "cell"
    private var articleListVM: ArticleListViewModel!

    //MARK: - Table View
    
    let newsTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorColor = .darkGray
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        // Dark Mode
        if #available(iOS 13.0, *) {
            tableView.backgroundColor = .systemBackground
        } else {
            tableView.backgroundColor = .white
        }
        return tableView
    }()
    
    //MARK: - Footer Notch View
    
    let footerNotchView: UIView = {
        let footerView = UIView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        footerView.backgroundColor = .navy
        return footerView
    }()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(newsTableView)
        view.addSubview(footerNotchView)
        
        WebService.getArticles { articles in
            if let articlesList = articles {
                self.articleListVM = ArticleListViewModel(articles: articlesList)
                
                DispatchQueue.main.async {
                    self.newsTableView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupStatusBar()
        setupLayout()
        setupTableView()
    }
    
    //MARK: - Methods
    
    private func setupStatusBar() {
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = .navy
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: (sharedApplication.delegate?.window??.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = .navy
            statusBar.tintColor = .white
            sharedApplication.delegate?.window??.addSubview(statusBar)
        } else {
            sharedApplication.statusBarView?.backgroundColor = .navy
        }
    }
    
    private func setupTableView() {
        title = "Good News II"
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.register(NewsTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - Setup Layout
    
    private func setupLayout() {
        // View
        view.backgroundColor = .white
        
        // TableView        
        NSLayoutConstraint.activate([
            newsTableView.topAnchor.constraint(equalTo: view.topAnchor),
            newsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            newsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            newsTableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -10)
        ])
        
        // Footer Notch View
        NSLayoutConstraint.activate([
            footerNotchView.topAnchor.constraint(equalTo: newsTableView.bottomAnchor),
            footerNotchView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            footerNotchView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            footerNotchView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
}

//MARK: - TableView Delegate

extension NewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - TableView DataSource

extension NewsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return articleListVM == nil ? 0 : articleListVM.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleListVM.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NewsTableViewCell
        
        let articleVM = articleListVM.articleAtIndex(indexPath.row)
           
        
        if #available(iOS 13.0, *) {
            cell!.backgroundColor = .systemBackground
        } else {
            cell!.backgroundColor = .white
        }
        cell!.title.textColor = .black
        cell!.title.text = articleVM.title
        cell!.notice.text = articleVM.description
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell else {
            fatalError("Cell is not of type NewsTableViewCell")
        }
        
        let articleVM = articleListVM.articleAtIndex(indexPath.row)
        delegate?.didTouch(cell: cell, indexPath: indexPath)
        let newsDetailVC = NewsDetailViewController()
        newsDetailVC.authorLabel.text = articleVM.author
        newsDetailVC.titleLabel.text = articleVM.title
        newsDetailVC.descriptionLabel.text = articleVM.description
        newsDetailVC.newsImage.image = UIImage(named: articleVM.image)
    }
    
}
