import UIKit

class NewsTableViewCell: UITableViewCell {

    let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        label.font = UIFont(name: "Avenir", size: 20)
        label.numberOfLines = 2
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    let notice: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .justified
        label.textColor = .red
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.font = UIFont(name: "Avenir", size: 16)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(title)
        contentView.addSubview(notice)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            
            notice.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 8),
            notice.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            notice.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            notice.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16)
        ])
    }

}
