import Foundation

struct ArticleList: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
}
