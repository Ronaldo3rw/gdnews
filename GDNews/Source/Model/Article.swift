import Foundation

// Representa um artigo
struct Article: Codable {
    let title: String?
    let description: String?
    let urlToImage: String?
    let author: String?
}
