import Foundation

struct ArticleListViewModel {
    let articles: [Article]
}

extension ArticleListViewModel {
    
    // Retorna o numero de sessões
    var numberOfSections: Int {
        return 1
    }
    
    // Retorna o numero de artigos de cada section(count)
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    // Retorna o artigo específico
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        let article = self.articles[index]
        return ArticleViewModel(article)
    }
}

struct ArticleViewModel {
    private let article: Article 
}

// Inicializa o artigo
extension ArticleViewModel {
    init(_ article: Article) {
        self.article = article
    }
}

extension ArticleViewModel {
    
    // Retorna o title e a description do artigo
    var title: String {
        return self.article.title ?? ""
    }
    
    var description: String {
        return self.article.description ?? ""
    }
    
    var author: String {
        return self.article.author ?? ""
    }
    
    var image: String {
        return self.article.urlToImage ?? ""
    }
    
}

