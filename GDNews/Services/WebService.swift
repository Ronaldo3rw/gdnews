import Foundation
import Alamofire

class WebService {
    
    static private let baseURL = "https://newsapi.org/v2/top-headlines"
    static private let country = "br"
    static private let apiKey = "e1b4dd5200694fb782af01beb556fcd5"
    
    class func getArticles(completion: @escaping ([Article]?) -> ()) {
        
        guard let url = URL(string: "\(baseURL)?country=\(country)&apiKey=\(apiKey)") else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                let articleList = try? JSONDecoder().decode(ArticleList.self, from: data)
                if let articleList = articleList {
                    completion(articleList.articles)
                }
            }
        }.resume()
    }
}
