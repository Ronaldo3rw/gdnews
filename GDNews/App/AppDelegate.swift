//
//  AppDelegate.swift
//  GDNews
//
//  Created by Ronaldo Dias on 15/07/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().backgroundColor = .navy
        UINavigationBar.appearance().barTintColor = .navy        
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white,
                                                                 .font: UIFont(name: "Avenir Next", size: 30 )!]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white,
                                                            .font: UIFont(name: "Avenir Next", size: 16)!]
                
        window = UIWindow(frame: UIScreen.main.bounds)        
        let navController = UINavigationController()
        let mainView = NewsViewController(nibName: nil, bundle: nil)
//        let mainView = ColapseViewController(nibName: nil, bundle: nil)
        navController.viewControllers = [mainView]
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        return true
    }


}

