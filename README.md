# Good News II
## _Projeto para estudo de programação Swift Avançada_

Este projeto foi desenvolvido para o estudo/treino de componentes avançados da programação em Swift para iOS.

## Features

- Home com uma lista de notícias carregadas da API
- Tela com os Detalhes da notícia contendo:

> - Imagem Principal da notícia. 
> - Título da notícia.
> - Texto com o conteúdo da notícia.
> - Créditos do autor da notícia. 
> - Data da publicação.

## Soluções Técnicas

### Linguagem
- Swift 5
- iOS Deployment Target: 14.5
- Técnica de programação View Code, sem utilização de Storyboards 

### Arquitetura
- MVVM
- Verificar a posibilidade da utilização do Design Pattern Coordinator

### Gerenciador de Dependências
- Swift Package Manager

### API
- Integração com a API RESTfull - [News API](https://newsapi.org/) 

## Adicionar posteriomente
> - Considerações finais 
> - Imagens ilustrativas

